#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#include "mandel.h"
#include "Image.h"

class bulkqueue : public queue {
private :
  int free_processor;
public :
  bulkqueue(MPI_Comm queue_comm,circle *workcircle)
    : queue(queue_comm,workcircle) {
    free_processor=0;
  };
  /* Send a coordinate to a free processor;
     if the coordinate is invalid, this should stop the process;
     otherwise add the result to the image.
  */
  void addtask(struct coordinate xy[], int p) {
    MPI_Status status[2*p]; int contribution[p]; MPI_Request reqs[2*p];

    /*for (int x=0; x<p; x++)
    {
    MPI_Isend(&xy[x],2,MPI_DOUBLE,x,0,comm, &reqs[x]);
    }
    for (int x=0; x<p; x++)
    {
    MPI_Irecv(&contribution[x],1,MPI_INT,x,0,comm, &reqs[x+p]);
    }
    MPI_Waitall(2*p, reqs, status);*/
	printf("About to scatter\n");
	int i,j,k,tmp;
	MPI_Comm_rank(MPI_COMM_WORLD,&tmp);
	double x[20];
	for(j=0,k=0; j < 11; j++,k=k+2)
	{
		x[k] = xy[j].x;
		x[k+1] = xy[j].y;
		printf("x[k] = %lf, x[k+1] = %lf\n",x[k],x[k+1]);
		printf("xy.x = %lf, xy.y = %lf\n",xy[j].x,xy[j].y);

	}
	printf("after xy.x = %lf, xy.y = %lf\n",xy[13].x,xy[13].y);
	printf("Sending from %d\n", tmp);
        /*x[0] = 1;
	x[1] = 2;
        x[2] = 3;
	x[3] = 4;
        x[4] = 5;
	x[5] = 6;
        x[6] = 7;
	x[7] = 8;
        x[8] = 9;
	x[9] = 10;
        x[10] = 11;
	x[11] = 12;
        x[12] = 11;
	x[13] = 12;
        x[14] = 11;
	x[15] = 12;
        x[16] = 11;
	x[17] = 12;
        x[18] = 11;
	x[19] = 12;*/
	MPI_Scatter(x,2, MPI_DOUBLE,NULL,0, MPI_DOUBLE,11, MPI_COMM_WORLD); 
	printf("Finished scattering, about to gather\n");
	MPI_Gather(NULL, 0, MPI_INT, &contribution,1,MPI_INT,11, MPI_COMM_WORLD);
    	printf("Finished gather, contribution = %d in host %d\n", contribution[0], tmp);
	MPI_Barrier(MPI_COMM_WORLD);

    for (int x=0; x<p; x++)
    {
    if (workcircle->is_valid_coordinate(xy[x]))
    {
      coordinate_to_image(xy[x],contribution[x]);
      total_tasks++;
    }
    }

    free_processor++;
    if (free_processor==ntids-1)
      // wrap around to the first again
      free_processor = 0;
  };
  void complete() {
    struct coordinate xy[ntids-1];
    workcircle->invalid_coordinate(xy[0]); free_processor=0;
        for(int p=1; p<ntids; p++)
        {
        //        xy[p] = xy[0];
        }
      addtask(xy, ntids-1);
    t_stop = MPI_Wtime();
    printf("Tasks %d in time %d\n",total_tasks,t_stop-t_start);
    image->Write();
    return;
  };
};


int main(int argc,char **argv) {
  MPI_Comm comm;
  int ntids,mytid, steps,iters,ierr;

  MPI_Init(&argc,&argv);
  comm = MPI_COMM_WORLD;
  MPI_Comm_size(comm,&ntids);
  MPI_Comm_rank(comm,&mytid);
  ierr = parameters_from_commandline
    (argc,argv,comm,&steps,&iters);
  if (ierr) return MPI_Abort(comm,1);

  if (ntids==1) {
    printf("Sorry, you need at least two processors\n");
    return 1;
  }

  circle *workcircle = new circle(2./steps,iters);
  bulkqueue *taskqueue = new bulkqueue(comm,workcircle);
  if (mytid==ntids-1)  {
    taskqueue->set_image( new Image(2*steps,2*steps,"mandelpicture") );
    for (;;) {
      struct coordinate xy[ntids];
      xy[ntids-1].x = -10; xy[ntids-1].y = -10;

      int p = 0;
      for (p = 0; p<ntids; p++)
      {
        workcircle->next_coordinate(xy[p]);

        if (!workcircle->is_valid_coordinate(xy[p]))
          break;
      }
      if(p==ntids-1)
               taskqueue->addtask(xy, p);
       else
       {
        taskqueue->addtask(xy, p);
        break;
      }

    }
    taskqueue->complete();
  } else
    taskqueue->wait_for_work(comm,workcircle);

  MPI_Finalize();
  return 0;
}

