#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <iostream>
using namespace std;
#include <mpi.h>

int main(int argc, char **argv){
int mytid,procs;
MPI_Comm comm;
int root = 0;
MPI_Init(&argc,&argv);
comm = MPI_COMM_WORLD;
MPI_Comm_rank(comm,&mytid);
MPI_Comm_size(MPI_COMM_WORLD,&procs);

{
  double tstart,tstop,jitter,sd;
  void *sendbuf;
  int wait;
  double averagejitter;
  wait = (int) (6.*rand() / (double)RAND_MAX);
  tstart = MPI_Wtime();
  sleep(wait);
  tstop = MPI_Wtime();
  jitter = tstop - tstart - wait;;

  if(mytid == 0)
 {
  sendbuf = MPI_IN_PLACE;}
 else
{ sendbuf = (void*)&jitter;}
printf("original jitter : %e %d \n", jitter, mytid);
MPI_Reduce(sendbuf,(void*)&averagejitter,1,MPI_DOUBLE,MPI_SUM,comm);
averagejitter = jitter/procs;
if(mytid == 0)
{
  printf("The average of the data set is %e \n",averagejitter);
}
averagejitter = jitter - averagejitter;
averagejitter = averagejitter * averagejitter;
MPI_Reduce((void*)&averagejitter,(void*)&sd,1,MPI_DOUBLE,MPI_SUM,0,comm);
if(mytid == 0)
{
	sd = sd/procs;
	sd = sqrt(sd);
	printf("Standard deviation = %e\n",sd);
}
}
MPI_Finalize();
return 0;
}
