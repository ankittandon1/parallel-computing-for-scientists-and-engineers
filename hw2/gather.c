 #include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
using namespace std;
#include <mpi.h>

int main(int argc,char **argv) {
  int mytid,procs;
  MPI_Comm comm;
  int root = 0;
  MPI_Init(&argc,&argv);
  comm = MPI_COMM_WORLD;
  MPI_Comm_rank(comm,&mytid);
  MPI_Comm_size(MPI_COMM_WORLD, &procs);


  {
    double tstart,tstop,jitter,sd;
    void *sendbuf;
    double *receivebuffer;
    int wait;
    double averagejitter;
   // wait for a random time, and measure how long
    wait = (int) ( 6.*rand() / (double)RAND_MAX );
    tstart = MPI_Wtime();
    sleep(wait);
    tstop = MPI_Wtime();
    jitter = tstop-tstart-wait;

    // find the maximum time over all processors
    if (mytid==0)
     {
	sendbuf = (void*)&jitter;
	receivebuffer = (double*)malloc(sizeof(sendbuf)*sizeof(double));
     }
    else
     {
        sendbuf = (void*)&jitter;
     }
     MPI_Gather(sendbuf,1,MPI_DOUBLE,receivebuffer,1,MPI_DOUBLE,0,comm);
     if(mytid==0){
	for(int i = 0; i < procs; i++)
	{
	    printf("gather values %e  %d \n",receivebuffer[i],i);
	}
	} 
}
  MPI_Finalize();
  return 0;
}

 
