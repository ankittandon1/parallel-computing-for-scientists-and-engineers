#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

int main(int argc,char **argv) {
  MPI_Comm comm;
  int ntids,mytid;
  MPI_Init(&argc,&argv);
  comm = MPI_COMM_WORLD;
  MPI_Comm_size(comm,&ntids);
  MPI_Comm_rank(comm,&mytid);

  {
    MPI_Status status;
    int my_number = mytid, other_number=-1.;
//************************** my implmentation ************************
	int proc_s=0, proc_r=0;
	MPI_Win win;
   	proc_s = mytid+1;
	
	if(mytid == ntids-1)
	{
		proc_s = MPI_PROC_NULL;
	}

	MPI_Win_create(&other_number, sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &win); 
	MPI_Win_fence(MPI_MODE_NOPRECEDE,win);
	MPI_Put(&my_number, 1, MPI_INT, proc_s, 0, 1, MPI_INT, win);
	MPI_Win_fence(0,win);
  	MPI_Win_free(&win);

//**********************************************************************
    /* Correctness check */
    int *gather_buffer=NULL;
    if (mytid==0) {
      gather_buffer = (int*) malloc(ntids*sizeof(int));
      if (!gather_buffer) MPI_Abort(comm,1);
    }
    MPI_Gather(&other_number,1,MPI_INT,
               gather_buffer,1,MPI_INT, 0,comm);
    if (mytid==0) {
      int i,error=0;
      for (i=0; i<ntids; i++) 
        if (gather_buffer[i]!=i-1) {
          printf("Processor %d was incorrect: %d should be %d\n",
                 i,gather_buffer[i],i-1);
          error =1;
        }
      if (!error) printf("Success!\n");
      free(gather_buffer);
    }
  }
  MPI_Finalize();
  return 0;
}
